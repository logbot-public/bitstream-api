# Accesso API Bitstream

Questa repository contiene degli esempi su come potersi autenticare ai servizi Logbot e su come utilizzare le API Bitstream.<br />
La cartella `src/auth` contiene codice su come ottenere access e refresh token e su come eseguire il refresh dell'access token.<br />
In particolare:
* `http-requests.js`: utilizza semplici chiamate HTTP;
* `oicd-library`: utilizza una libreria che supporta il protocollo OpenID Connect (OIDC).

La cartella `src/bitstream` contiene codice su come eseguire chiamate autenticate al servizio Logbot Bitstream.
In particolare:
* `metrics-json.js`: esegue chiamata `GET /bitstream/metrics/index.json`;
* `metrics-find.js`: esegue chiamate `GET /bitstream/metrics/find`;
* `render.js`: esegue chiamate `GET /bitstream/render`.

Infine il file `src/scripts.md` contiene delle chiamate `curl` per ottenere l'access token, per aggiornare l'access token e una chiamata ai servizi Bitstream. 

## Prerequisiti
1. Assicurarsi che la propria utenza abbia i permessi per poter accedere alle API Bitstream;
2. Creare una copia del file `.example.env`, rinominandola `.env`, modificando le seguenti variabili:
  * __DEMO_LOGBOT_DOMAIN__: Dominio Logbot. Impostare a `logbotiot.cloud` ;
  * __DEMO_CLIENT_ID__: Client ID del servizio di autenticazione Logbot ;
  * __DEMO_CLIENT_SECRET__: Client secret del servizio di autenticazione Logbot ;
  * __DEMO_USERNAME__: Vostro username ;
  * __DEMO_PASSWORD__: Vostra password .
3. Assicurarsi di avere installato la versione __16__ di `node` e la versione __8__ di `npm` ;
4. Eseguire il comando `npm install` .

## Esecuzione scripts
Eseguire i comandi:
* `npm run auth-http` per lanciare lo script di autenticazione tramite richieste HTTP;
* `npm run auth-oidc` per lanciare lo script di autenticazione tramite libreria OIDC;
* `npm run bit-index` per lanciare lo script che eseguira una chiamata HTTP verso `/bitstream/metrics/index.json`;
* `npm run bit-find` per lanciare lo script che eseguira una chiamata HTTP verso `/bitstream/metrics/find`;
* `npm run bit-render` per lanciare lo script che eseguira una chiamata HTTP verso `/bitstream/render`.
