# Autenticazione
```
curl \
-X POST \
-H "Content-Type: application/x-www-form-urlencoded" \
--data "grant_type=password&client_id=<CLIENT_ID>&client_secret=<CLIENT_SECRET>&username=<USERNAME>&password=<PASSWORD>" \
https://iam.logbotiot.cloud/auth/realms/master/protocol/openid-connect/token
```

# Refresh Access Token
```
curl \
-X POST \
-H "Content-Type: application/x-www-form-urlencoded" \
--data "grant_type=refresh_token&client_id=<CLIENT_ID>&client_secret=<CLIENT_SECRET>&refresh_token=<REFRESH_TOKEN>" \
https://iam.logbotiot.cloud/auth/realms/master/protocol/openid-connect/token
```

# Bitstream /metrics/index.json
```
curl \
-X GET \
-H "Authorization: Bearer <ACCESS_TOKEN>" \
https://platform.logbotiot.cloud/bitstream/metrics/index.json
```
