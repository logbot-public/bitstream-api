import 'dotenv/config'; 
import { Issuer } from 'openid-client';

// Params
const oidcEndpoint = `https://iam.${process.env.DEMO_LOGBOT_DOMAIN}/auth/realms/master`;
const clientId = process.env.DEMO_CLIENT_ID;
const clientSecret = process.env.DEMO_CLIENT_SECRET;
const username = process.env.DEMO_USERNAME;
const password = process.env.DEMO_PASSWORD;

console.log("OIDC Endpoint: %s", oidcEndpoint);
console.log("Client ID: %s", clientId);
console.log("Client Secret: %s", clientSecret);
console.log("Username: %s", username);
console.log("Password: %s", password);

try {
  const logbotIssuer = await Issuer.discover(oidcEndpoint);
  console.log('Discovered issuer %s %O', logbotIssuer.issuer, logbotIssuer.metadata);
  const client = new logbotIssuer.Client({
    client_id: clientId,
    client_secret: clientSecret,
    response_types: ['code'],
  });

  let tokenSet = await client.grant({
    grant_type: 'password',
    username: username,
    password: password
  });

  console.log("Access response: %O", tokenSet);
  
  tokenSet = await client.refresh(tokenSet.refresh_token);

  console.log("Refresh response: %O", tokenSet);
}
catch (error) {
  console.log('Error: %s', error.message);
}
