import 'dotenv/config'; 
import axios from 'axios';
import { stringify } from 'querystring';

// Params
const tokenEndpoint = `https://iam.${process.env.DEMO_LOGBOT_DOMAIN}/auth/realms/master/protocol/openid-connect/token`;
const clientId = process.env.DEMO_CLIENT_ID;
const clientSecret = process.env.DEMO_CLIENT_SECRET;
const username = process.env.DEMO_USERNAME;
const password = process.env.DEMO_PASSWORD;

console.log("Token Endpoint: %s", tokenEndpoint);
console.log("Client ID: %s", clientId);
console.log("Client Secret: %s", clientSecret);
console.log("Username: %s", username);
console.log("Password: %s", password);

try {
  let authParams = {
    grant_type: 'password',
    client_id: clientId,
    client_secret: clientSecret,
    username: username,
    password: password,
  };
  console.log("Stringified params: %s", stringify(authParams));

  // Request Access and Refresh tokens
  const { data } = await axios.post(tokenEndpoint, stringify(authParams), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded', 
    }
  });
  console.log("Access response: %O", data);

  // Refresh access token
  const refreshParams = {
    grant_type: 'refresh_token',
    client_id: clientId,
    client_secret: clientSecret,
    refresh_token: data.refresh_token,
  }
  const res = await axios.post(tokenEndpoint, stringify(refreshParams), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded', 
    }
  });
  console.log("Refresh response: %O", res.data);
}
catch (error) {
  console.log('Error: %s', error.message);
}
