import 'dotenv/config';
import { Issuer } from 'openid-client';
import axios from 'axios';

// Auth parameters
const oidcEndpoint = `https://iam.${process.env.DEMO_LOGBOT_DOMAIN}/auth/realms/master`;
const clientId = process.env.DEMO_CLIENT_ID;
const clientSecret = process.env.DEMO_CLIENT_SECRET;
const username = process.env.DEMO_USERNAME;
const password = process.env.DEMO_PASSWORD;

// Resource parameters
const resource = `https://platform.${process.env.DEMO_LOGBOT_DOMAIN}/bitstream/tags/autoComplete/values`;
const tag = "name";
// Is it possible to specify more values for expr (e.g.: expr=A&expr=B&expr=C)
const expr = undefined;
//const expr = "lbrep=agg10y";
//const expr = "name=~IoT_1.*";
//const expr = "name=~IoT_1.(Conn1|Conn2|Conn3).*";
const valuePrefix = undefined;
//const valuePrefix = "IoT_1";


console.log("OIDC Endpoint: %s", oidcEndpoint);
console.log("Client ID: %s", clientId);
console.log("Client Secret: %s", clientSecret);
console.log("Username: %s", username);
console.log("Password: %s", password);
console.log("Resource: %s", resource);
console.log("Tag: %s", tag);
if (expr != undefined) {
  console.log("Expr: %s", resource);
}
if (valuePrefix != undefined) {
  console.log("ValuePrefix: %s", valuePrefix);
}

try {
  const logbotIssuer = await Issuer.discover(oidcEndpoint);
  const client = new logbotIssuer.Client({
    client_id: clientId,
    client_secret: clientSecret,
    response_types: ['code'],
  });

  const tokenSet = await client.grant({
    grant_type: 'password',
    username: username,
    password: password
  });
  
  let options = {
    headers: {
      Authorization: `Bearer ${tokenSet.access_token}`,
    },
    params: {
      tag: tag,
    },
  };
  // If expr != undefined
  if (expr != undefined) {
    options.params['expr'] = expr;
  }
  if (valuePrefix != undefined) {
    options.params['valuePrefix'] = valuePrefix;
  }
  const { data } = await axios.get(resource, options);
  console.log('Response: %O', data);

}
catch (error) {
  console.log('Error: %s', error.message);
}
