import 'dotenv/config';
import { Issuer } from 'openid-client';
import axios from 'axios';

// Auth parameters
const oidcEndpoint = `https://iam.${process.env.DEMO_LOGBOT_DOMAIN}/auth/realms/master`;
const clientId = process.env.DEMO_CLIENT_ID;
const clientSecret = process.env.DEMO_CLIENT_SECRET;
const username = process.env.DEMO_USERNAME;
const password = process.env.DEMO_PASSWORD;

// Resource parameters
const resource = `https://platform.${process.env.DEMO_LOGBOT_DOMAIN}/bitstream/tags/autoComplete/tags`;
const tagPrefix = undefined;

console.log("OIDC Endpoint: %s", oidcEndpoint);
console.log("Client ID: %s", clientId);
console.log("Client Secret: %s", clientSecret);
console.log("Username: %s", username);
console.log("Password: %s", password);
console.log("Resource: %s", resource);
if (tagPrefix != undefined) {
  console.log("Tag Prefix: %s", tagPrefix);
}

try {
  const logbotIssuer = await Issuer.discover(oidcEndpoint);
  const client = new logbotIssuer.Client({
    client_id: clientId,
    client_secret: clientSecret,
    response_types: ['code'],
  });

  const tokenSet = await client.grant({
    grant_type: 'password',
    username: username,
    password: password
  });
  
  const options = {
    headers: {
      Authorization: `Bearer ${tokenSet.access_token}`,
    },
  };

  if (tagPrefix) {
    options['params'] = {
      tagPrefix: tagPrefix,
    };
  }

  const { data } = await axios.get(resource, options);
  console.log('Response: %O', data);

}
catch (error) {
  console.log('Error: %s', error.message);
}
