import 'dotenv/config';
import { Issuer } from 'openid-client';
import axios from 'axios';

// Auth parameters
const oidcEndpoint = `https://iam.${process.env.DEMO_LOGBOT_DOMAIN}/auth/realms/master`;
const clientId = process.env.DEMO_CLIENT_ID;
const clientSecret = process.env.DEMO_CLIENT_SECRET;
const username = process.env.DEMO_USERNAME;
const password = process.env.DEMO_PASSWORD;

// Resource parameters
const resource = `https://platform.${process.env.DEMO_LOGBOT_DOMAIN}/bitstream/render`;
const target = `seriesByTag('name=<METRIC_ID>')`;
const from = 'now-5m';
const until = 'now';
const maxDataPoints = undefined;

console.log("OIDC Endpoint: %s", oidcEndpoint);
console.log("Client ID: %s", clientId);
console.log("Client Secret: %s", clientSecret);
console.log("Username: %s", username);
console.log("Password: %s", password);
console.log("Resource: %s", resource);
console.log("Target: %s", target);
console.log("From: %s", from);
console.log("Until: %s", until);
if (maxDataPoints != undefined) {
  console.log("MaxDataPoints: %d", maxDataPoints);
}

try {
  const logbotIssuer = await Issuer.discover(oidcEndpoint);
  const client = new logbotIssuer.Client({
    client_id: clientId,
    client_secret: clientSecret,
    response_types: ['code'],
  });

  const tokenSet = await client.grant({
    grant_type: 'password',
    username: username,
    password: password
  });
  
  const options = {
    headers: {
      Authorization: `Bearer ${tokenSet.access_token}`,
    },
    params: {
      target: target,
      from: from,
      until: until,
    }
  };
  if (maxDataPoints != undefined) {
    options.params['maxDataPoints'] = maxDataPoints;
  }
  const { data } = await axios.get(resource, options);
  console.log('Response: %O', data);
  const datapoints = data[0].datapoints;
  console.log("Number of points for the 1st metric: %d", datapoints.length);
  for (let i = 0; i < datapoints.length; i++) {
    console.log("Valore:%d Timestamp: %d", datapoints[i][0], datapoints[i][1]);
  }

}
catch (error) {
  console.log('Error: %s', error.message);
}
